package com.fcm.supplier.serviceImpl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fcm.supplier.service.FileStorageService;

@Service
public class FileStorageServiceImpl implements FileStorageService {
	
	
    private static String UPLOADED_FOLDER = "C://fcm//";
    
    public final static SimpleDateFormat fileName=new SimpleDateFormat("yyyyMMddHHmm");

	 public String storeFile(MultipartFile file){
		// Normalize file name
	        String fileName1 = StringUtils.cleanPath(file.getOriginalFilename());
            String fileName2 = fileName.format(new Date());
            
          	 int l=file.getOriginalFilename().lastIndexOf(".");
          	 fileName1=file.getOriginalFilename().substring(0,l)+(fileName2);
          	 System.out.println(fileName1);
	        try {     // Check if the file's name contains invalid characters
	            if(fileName1.contains("..")) {
	                
	            }

	            // Copy file to the target location (Replacing existing file with the same name)
	            
	            Path targetLocation = Paths.get(UPLOADED_FOLDER + fileName1);;
	            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

	          //  return fileName;
	        } catch (IOException ex) {
	           
	        }
			return fileName1;
	 }
}
