package com.fcm.supplier.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fcm.supplier.entity.Vendor;
import com.fcm.supplier.repository.VendorRepository;
import com.fcm.supplier.service.VendorService;
@Service
public class VendorServiceImpl implements VendorService {

	@Autowired
	VendorRepository vendorRepository;
	public Vendor findByEmail(String vendorEmail) {
		// TODO Auto-generated method stub
		return vendorRepository.findByVendorEmail(vendorEmail);
	}
	public Vendor save(Vendor vendor1) {
		// TODO Auto-generated method stub
		return vendorRepository.save(vendor1);
	}
	public List<Vendor> findAll() {
		// TODO Auto-generated method stub
		return vendorRepository.findAll();
	}
	public Optional<Vendor> findById(Long id) {
		// TODO Auto-generated method stub
		return vendorRepository.findById(id);
	}
	public void deleteById(long id) {
		// TODO Auto-generated method stub
	     vendorRepository.deleteById(id);;
	}

}
