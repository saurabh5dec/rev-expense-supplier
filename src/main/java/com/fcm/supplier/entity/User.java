package com.fcm.supplier.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;
	    
		private String username;
		@Column(name = "employee_name")
		private String employeeName;
		@Column(name = "employee_id")
		private String employeeId;
		
		@Column(name = "email", nullable = false, unique = true)
		private String email;
		private String password;
		@Column(name = "enabled")
		private boolean enabled;
		@Column(name = "created_on")
		private Date createdOn;
//		@JsonIgnore
//		@ManyToMany(fetch = FetchType.LAZY)
//		@JoinTable(name = "usersroles", joinColumns = @JoinColumn(name = "userId", referencedColumnName = "id"),
//		inverseJoinColumns = @JoinColumn(name = "roleId", referencedColumnName = "roleId"))
//		private Collection<Role> roles;
//		
		@Column(name="cost_center")
		private String costCenter;
		
		@Column(name="approvedBy")
		private String approvedBy;
		@Column(name="approvedEmail")
		private String approvedEmail;
		@Column(name="branch")
		private String branch;
		
		
		
		
		public String getBranch() {
			return branch;
		}
		public void setBranch(String branch) {
			this.branch = branch;
		}
		public String getApprovedBy() {
			return approvedBy;
		}
		public void setApprovedBy(String approvedBy) {
			this.approvedBy = approvedBy;
		}
		public String getApprovedEmail() {
			return approvedEmail;
		}
		public void setApprovedEmail(String approvedEmail) {
			this.approvedEmail = approvedEmail;
		}
		public String getCostCenter() {
			return costCenter;
		}
		public void setCostCenter(String costCenter) {
			this.costCenter = costCenter;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		
		public String getEmployeeId() {
			return employeeId;
		}
		public void setEmployeeId(String employeeId) {
			this.employeeId = employeeId;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public boolean isEnabled() {
			return enabled;
		}
		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
		public Date getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
//		public Collection<Role> getRoles() {
//			return roles;
//		}
//		public void setRoles(Collection<Role> roles) {
//			this.roles = roles;
//		}
		public String getEmployeeName() {
			return employeeName;
		}
		public void setEmployeeName(String employeeName) {
			this.employeeName = employeeName;
		}
		
		

}
