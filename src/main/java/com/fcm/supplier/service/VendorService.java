package com.fcm.supplier.service;

import java.util.List;
import java.util.Optional;

import com.fcm.supplier.entity.SupplierForm;
import com.fcm.supplier.entity.Vendor;

public interface VendorService {
	public Vendor findByEmail(String vendorEmail);
	public Vendor save(Vendor vendor1);
	public List<Vendor> findAll();
   public Optional<Vendor> findById(Long id);
   public void deleteById(long id);
  
}
