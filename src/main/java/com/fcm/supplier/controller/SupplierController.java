package com.fcm.supplier.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fcm.supplier.entity.SupplierForm;
import com.fcm.supplier.entity.User;
import com.fcm.supplier.entity.Vendor;
import com.fcm.supplier.serviceImpl.FileStorageServiceImpl;
import com.fcm.supplier.serviceImpl.MailServiceImpl;
import com.fcm.supplier.serviceImpl.SupplierServiceImpl;
import com.fcm.supplier.serviceImpl.UserServiceImpl;
import com.fcm.supplier.serviceImpl.VendorServiceImpl;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/supplier")
public class SupplierController {

	
	@Autowired
	private  MailServiceImpl mailServiceImpl;
	
	@Autowired
	VendorServiceImpl vendorServiceImpl;
	 @Autowired
	 private UserServiceImpl userServiceImpl;
	 @Autowired
	 private SupplierServiceImpl supplierServiceImpl;
	 @Autowired
	  private FileStorageServiceImpl fileStorageService;
	 
	 private static String DOWNLOADED_FOLDER = "C://fcm//";
	   
    /*
	    * this method used to get user  data by user id 
	    */
    @GetMapping(path = "/getUser/{id}")
	public ResponseEntity<User> getUser(@PathVariable Long id) {
    	Optional<User> user=userServiceImpl.findById(id);
    	return  ResponseEntity.ok(user.get());
    }
    
    
    
    
    
    @PostMapping(value = "/upload/{id}")
    public String uploadFile(@PathVariable Long id,@RequestParam(value="selectValue") String selectValue
     ,@RequestParam(value="amount") Float amount,@RequestParam(value="fromDate") String fromDate,
     @RequestParam(value="paymentMode") String paymentMode,@RequestParam(value="supplierType") String supplierType, 
     @RequestParam(value="beneficiaryName") String beneficiaryName, @RequestParam(value="invoiceNumber") String invoiceNumber,
     @RequestParam(value="approvedBy") String approvedBy,
     @RequestParam(value="description") String description,@RequestParam(value ="file") MultipartFile[] file) throws ParseException {
		 String uniqueCode=null;
		 SupplierForm supplierForm=new SupplierForm();
		  Optional<User> user=userServiceImpl.findById(id);
		  if(user.isPresent()){
	     String fileDownloadUri = ServletUriComponentsBuilder.fromUriString("/download/").toUriString();
	     supplierForm.setDownloadPath(fileDownloadUri);
	     supplierForm.setAmount(amount);
	     supplierForm.setInvoiceNo(invoiceNumber);
	     supplierForm.setApprovedBy(approvedBy);
	     supplierForm.setName(user.get().getEmployeeName());
	     supplierForm.setBranch(user.get().getBranch());
	     supplierForm.setEmail(user.get().getEmail());
	     String sDate1=fromDate;  
	     Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);  
	     DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd ");  
			String strDate = dateFormat.format(date1); 
	     supplierForm.setDueDate(strDate);
	     supplierForm.setSubDate(strDate);
	     supplierForm.setInvoiceDate(strDate);
	     supplierForm.setEmployeeId(user.get().getEmployeeId());
	     supplierForm.setBranch(user.get().getBranch());
	     supplierForm.setTransType(paymentMode);
	     supplierForm.setPaymentStatus("Pending");
	     supplierForm.setFreeText1(description);
    	  for (int i = 0; i < file.length; i++) {
              System.out.println("File name '%s' uploaded successfully."+file[i].getOriginalFilename());
              if(i==0){
            	  String fileName = fileStorageService.storeFile(file[i]);
            	  supplierForm.setFileName(fileName);
              }else{
                String fileName = fileStorageService.storeFile(file[i]);
                supplierForm.setInvFileName(fileName);
              }
         }
    	   uniqueCode=userServiceImpl.getRandomString(9);
    	   supplierForm.setRefNo(uniqueCode);
    	 
	}
		  supplierServiceImpl.save(supplierForm);
	     mailServiceImpl.sendSimpleMessage(user.get().getApprovedEmail(),"Request Approved For Expense"," user can  applied for expense");

   return uniqueCode;	 
    	  
}
    
    /**
	    * this method return all the apply data of requestForm
	    * @param id
	    * @return
	    */
		@GetMapping(path = "/findAllApplyData/{id}")
		public ResponseEntity<List<SupplierForm>> findAllApplyData(@PathVariable Long id) {
			List<SupplierForm> supplierList=new ArrayList<SupplierForm>();
			 Optional<User> user=userServiceImpl.findById(id);
			  if(user.isPresent()){
	    	List<SupplierForm> requestForm=supplierServiceImpl.findByEmployeeId(user.get().getEmployeeId());
	    	for(SupplierForm supplierForm:requestForm){
	    	if(supplierForm.getPaymentStatus().equalsIgnoreCase("Pending")){
	    		supplierList.add(supplierForm);
	    	return  ResponseEntity.ok(supplierList);
	    	}
	    	}
			  }
	    	return  ResponseEntity.ok(null);
	    }
		
		

		@PostMapping(value="/addVendor")
		public ResponseEntity<?> addVendor(@RequestBody Vendor vendor){
			System.out.println("testts"+vendor.getBankAccount());
			Vendor vendor2=new Vendor();
			Vendor vendor1=vendorServiceImpl.findByEmail(vendor.getVendorEmail());
			if(vendor1==null){
				vendor2.setBankAccount(vendor.getBankAccount());
				vendor2.setGst(vendor.getGst());
				vendor2.setIfscCode(vendor.getIfscCode());
				vendor2.setPan(vendor.getPan());
				vendor2.setVendorAddress(vendor.getVendorAddress());
				vendor2.setVendorCode(vendor.getVendorCode());
				vendor2.setVendorContact(vendor.getVendorContact());
				vendor2.setVendorEmail(vendor.getVendorEmail());
				vendor2.setVendorName(vendor.getVendorName());
				vendorServiceImpl.save(vendor2);
				return  ResponseEntity.ok(vendor2);
			}
			return ResponseEntity.ok("Vendor already exist");
		}
		
		@GetMapping(value="/getAllVendor")
		public List<Vendor> getAllVendor(){
			return vendorServiceImpl.findAll(); 
		}
		
		@DeleteMapping(value="/deleteById/{id}")
		public void deleteById(@PathVariable Long id){
			Optional<Vendor> vendor=vendorServiceImpl.findById(id);
			if(vendor.isPresent()){
		      vendorServiceImpl.deleteById(vendor.get().getId());
			}
		}
		
		@PutMapping(value="/updateVendor")
		public String updateVendor(@RequestBody Vendor vendor){
			Optional<Vendor> vendor1=vendorServiceImpl.findById(vendor.getId());
			if(vendor1.isPresent()){
				if(vendor.getBankAccount()!=null)
				vendor1.get().setBankAccount(vendor.getBankAccount());
				if(vendor.getVendorAddress()!=null)
				vendor1.get().setVendorAddress(vendor.getVendorAddress());
				if(vendor.getIfscCode()!=null)
				vendor1.get().setIfscCode(vendor.getIfscCode());
			vendorServiceImpl.save(vendor1.get());
			return "Update SucessFully";
			}
			return "Vendor not Update";
		}
		
		
		 @GetMapping("/download/")
		    public ResponseEntity<Resource> downloadInvoiceFile(@RequestParam(value="fileName") String fileName) throws IOException {
		      Path path = Paths.get(DOWNLOADED_FOLDER + java.io.File.separator + fileName);
		      String mimeType = Files.probeContentType(path);
		      Resource resource = null;
		     try {
		       resource = new UrlResource(path.toUri());
		       } catch (MalformedURLException e) {

		       }
		     return ResponseEntity.ok().contentType(MediaType.parseMediaType(mimeType))
		       .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
		        .body(resource);

		}	
		 
		 
		 
		 @PutMapping(value="/updateStatus") 
		 public void updateStatus(@RequestBody SupplierForm supplierForm) {
			 SupplierForm supplierForm2=supplierServiceImpl.findByRefNo(supplierForm.getRefNo());
			if(supplierForm2!=null){
				supplierForm2.setPaymentStatus(supplierForm.getPaymentStatus());
				supplierForm2.setRejectedReason(supplierForm.getRejectedReason());
		     	supplierServiceImpl.save(supplierForm2);
		     	Optional<User> user=userServiceImpl.findByEmployeeId(supplierForm.getEmployeeId());
		     	 mailServiceImpl.sendSimpleMessage(user.get().getEmail(),"Request Approved For Expense"," Request approved");
			}

		}
		   
		 @GetMapping(value = "/list/searchData", produces = "application/json")
			public ResponseEntity<?> getRequestFormDataBySearch(
					@RequestParam(value = "fromDate", required = false) Date fromDate,
					@RequestParam(value = "toDate", required = false) Date toDate,
					@RequestParam(value = "uniqueCode", required = false) String refNo,
					@RequestParam(value = "status", required = false) String PaymentStatus,
					@RequestParam(value = "name", required = false) String employeeId) throws ValidationException {
			
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd ");  
				String strDate = dateFormat.format(fromDate); 
				String strDate1 = dateFormat.format(toDate); 
				
				List<SupplierForm> requestFormList = supplierServiceImpl.findBySupplierForm(
						strDate.isEmpty() ? null :strDate,strDate1.isEmpty() ? null :strDate1,refNo.isEmpty() ? null :refNo,PaymentStatus.isEmpty() ? null :PaymentStatus,employeeId.isEmpty() ? null :employeeId);

				if (requestFormList.size() == 0) {
					throw new ValidationException("Data are not there");
				} else {
					return new ResponseEntity<>(requestFormList, HttpStatus.OK);
				}

			}
}
