package com.fcm.supplier.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="supplier_form")
public class SupplierForm {
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
			private long id;
	      @Column(name="refId")
			private long refId;
	      @Column(name="subDate")
            private String 	subDate;
	      @Column(name="name")
			private String name;
	      @Column(name="branch")
			private String	branch;
	      @Column(name="email")
			private String email;
	      @Column(name="dueDate")
			private String dueDate;
	      @Column(name="isBill")
			private String isBill;
	      @Column(name="utility")
			private String	utility;
	      @Column(name="transType")
			private String transType;
	      @Column(name="refNo")
			private String refNo;
	      @Column(name="drAccountNo")
			private String drAccountNo;
	      @Column(name="paymentNarration")
			private String	paymentNarration;
	      @Column(name="beneAdd")
			private String	beneAdd;
	      @Column(name="beneAdd1")
			private String	beneAdd1;
	      @Column(name="beneAdd2")
			private String	beneAdd2;
	      @Column(name="beneAdd3")
			private String	beneAdd3;
	      @Column(name="beneAdd4")
			private String	beneAdd4;
	      @Column(name="beneAdd5")
			private String beneAdd5;
	      @Column(name="paymentLocation")
			private String paymentLocation;
	      @Column(name="chequeNo")
			private String	chequeNo;
	      @Column(name="valueDate")
			private String	valueDate;
	      @Column(name="amount")
			private float amount;
	      @Column(name="print_Branch_Location")
			private String printBranchLocation;
	      @Column(name="emailAdd1")
			private String emailAdd1;
	      @Column(name="emailAdd2")
			private String emailAdd2;
	      @Column(name="emailAdd3")
			private String emailAdd3;
	      @Column(name="freeText1")
			private String freeText1;
	      @Column(name="NA1")
			private String NA1;
	      @Column(name="NA2")
			private String NA2;
	      @Column(name="NA3")
			private String NA3;
	      @Column(name="NA4")
			private String NA4;
	      @Column(name="NA5")
			private String NA5;
	      @Column(name="account_No")
			private String	accountNo;
	      @Column(name="IFSC")
			private String IFSC;
	      @Column(name="NA6")
			private String NA6;
	      @Column(name="deliver_To")
			private String	deliverTo;
	      @Column(name="bank_Name")
			private String bankName;
	      @Column(name="bank_Type")
			private String bankType;
	      @Column(name="bank_address")
			private String bankaddress;
	      @Column(name="invoice_No")
			private String invoiceNo;
	      @Column(name="under_Process")
			private String underProcess;
	      @Column(name="rejected_Reason")
			private String rejectedReason;
	      @Column(name="payment_Status")
			private String paymentStatus;
	      @Column(name="bank_File")
			private String bankFile;
	      @Column(name="tDS_Deduction")
			private String tDSDeduction;
	      @Column(name="other_Ded")
			private String otherDed;
	      @Column(name="remark")
			private String remark;
	      @Column(name="approvedBy")
			private String approvedBy;
	      @Column(name="download_Path")
			private String downloadPath;
	      @Column(name="file_Name")
			private String fileName;
	      @Column(name="inv_File_Name")
			private String invFileName;
	      @Column(name="invoice_Date")
			private String invoiceDate;
	      @Column(name="narration")
	        private String 	narration;
	      @Column(name="invoice_Type")
			private String invoiceType;
	       @Column(name="employee_id")
			private String employeeId;
	       
		  public String getEmployeeId() {
			return employeeId;
		}
		public void setEmployeeId(String employeeId) {
			this.employeeId = employeeId;
		}
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public long getRefId() {
			return refId;
		}
		public void setRefId(long refId) {
			this.refId = refId;
		}
		public String getSubDate() {
			return subDate;
		}
		public void setSubDate(String subDate) {
			this.subDate = subDate;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getBranch() {
			return branch;
		}
		public void setBranch(String branch) {
			this.branch = branch;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getDueDate() {
			return dueDate;
		}
		public void setDueDate(String dueDate) {
			this.dueDate = dueDate;
		}
		public String getIsBill() {
			return isBill;
		}
		public void setIsBill(String isBill) {
			this.isBill = isBill;
		}
		public String getUtility() {
			return utility;
		}
		public void setUtility(String utility) {
			this.utility = utility;
		}
		public String getTransType() {
			return transType;
		}
		public void setTransType(String transType) {
			this.transType = transType;
		}
		public String getRefNo() {
			return refNo;
		}
		public void setRefNo(String refNo) {
			this.refNo = refNo;
		}
		public String getDrAccountNo() {
			return drAccountNo;
		}
		public void setDrAccountNo(String drAccountNo) {
			this.drAccountNo = drAccountNo;
		}
		public String getPaymentNarration() {
			return paymentNarration;
		}
		public void setPaymentNarration(String paymentNarration) {
			this.paymentNarration = paymentNarration;
		}
		public String getBeneAdd() {
			return beneAdd;
		}
		public void setBeneAdd(String beneAdd) {
			this.beneAdd = beneAdd;
		}
		public String getBeneAdd1() {
			return beneAdd1;
		}
		public void setBeneAdd1(String beneAdd1) {
			this.beneAdd1 = beneAdd1;
		}
		public String getBeneAdd2() {
			return beneAdd2;
		}
		public void setBeneAdd2(String beneAdd2) {
			this.beneAdd2 = beneAdd2;
		}
		public String getBeneAdd3() {
			return beneAdd3;
		}
		public void setBeneAdd3(String beneAdd3) {
			this.beneAdd3 = beneAdd3;
		}
		public String getBeneAdd4() {
			return beneAdd4;
		}
		public void setBeneAdd4(String beneAdd4) {
			this.beneAdd4 = beneAdd4;
		}
		public String getBeneAdd5() {
			return beneAdd5;
		}
		public void setBeneAdd5(String beneAdd5) {
			this.beneAdd5 = beneAdd5;
		}
		public String getPaymentLocation() {
			return paymentLocation;
		}
		public void setPaymentLocation(String paymentLocation) {
			this.paymentLocation = paymentLocation;
		}
		public String getChequeNo() {
			return chequeNo;
		}
		public void setChequeNo(String chequeNo) {
			this.chequeNo = chequeNo;
		}
		public String getValueDate() {
			return valueDate;
		}
		public void setValueDate(String valueDate) {
			this.valueDate = valueDate;
		}
		public float getAmount() {
			return amount;
		}
		public void setAmount(float amount) {
			this.amount = amount;
		}
		public String getPrintBranchLocation() {
			return printBranchLocation;
		}
		public void setPrintBranchLocation(String printBranchLocation) {
			this.printBranchLocation = printBranchLocation;
		}
		public String getEmailAdd1() {
			return emailAdd1;
		}
		public void setEmailAdd1(String emailAdd1) {
			this.emailAdd1 = emailAdd1;
		}
		public String getEmailAdd2() {
			return emailAdd2;
		}
		public void setEmailAdd2(String emailAdd2) {
			this.emailAdd2 = emailAdd2;
		}
		public String getEmailAdd3() {
			return emailAdd3;
		}
		public void setEmailAdd3(String emailAdd3) {
			this.emailAdd3 = emailAdd3;
		}
		public String getFreeText1() {
			return freeText1;
		}
		public void setFreeText1(String freeText1) {
			this.freeText1 = freeText1;
		}
		public String getNA1() {
			return NA1;
		}
		public void setNA1(String nA1) {
			NA1 = nA1;
		}
		public String getNA2() {
			return NA2;
		}
		public void setNA2(String nA2) {
			NA2 = nA2;
		}
		public String getNA3() {
			return NA3;
		}
		public void setNA3(String nA3) {
			NA3 = nA3;
		}
		public String getNA4() {
			return NA4;
		}
		public void setNA4(String nA4) {
			NA4 = nA4;
		}
		public String getNA5() {
			return NA5;
		}
		public void setNA5(String nA5) {
			NA5 = nA5;
		}
		public String getAccountNo() {
			return accountNo;
		}
		public void setAccountNo(String accountNo) {
			this.accountNo = accountNo;
		}
		public String getIFSC() {
			return IFSC;
		}
		public void setIFSC(String iFSC) {
			IFSC = iFSC;
		}
		public String getNA6() {
			return NA6;
		}
		public void setNA6(String nA6) {
			NA6 = nA6;
		}
		public String getDeliverTo() {
			return deliverTo;
		}
		public void setDeliverTo(String deliverTo) {
			this.deliverTo = deliverTo;
		}
		public String getBankName() {
			return bankName;
		}
		public void setBankName(String bankName) {
			this.bankName = bankName;
		}
		public String getBankType() {
			return bankType;
		}
		public void setBankType(String bankType) {
			this.bankType = bankType;
		}
		public String getBankaddress() {
			return bankaddress;
		}
		public void setBankaddress(String bankaddress) {
			this.bankaddress = bankaddress;
		}
		public String getInvoiceNo() {
			return invoiceNo;
		}
		public void setInvoiceNo(String invoiceNo) {
			this.invoiceNo = invoiceNo;
		}
		public String getUnderProcess() {
			return underProcess;
		}
		public void setUnderProcess(String underProcess) {
			this.underProcess = underProcess;
		}
		public String getRejectedReason() {
			return rejectedReason;
		}
		public void setRejectedReason(String rejectedReason) {
			this.rejectedReason = rejectedReason;
		}
		public String getPaymentStatus() {
			return paymentStatus;
		}
		public void setPaymentStatus(String paymentStatus) {
			this.paymentStatus = paymentStatus;
		}
		public String getBankFile() {
			return bankFile;
		}
		public void setBankFile(String bankFile) {
			this.bankFile = bankFile;
		}
		public String gettDSDeduction() {
			return tDSDeduction;
		}
		public void settDSDeduction(String tDSDeduction) {
			this.tDSDeduction = tDSDeduction;
		}
		public String getOtherDed() {
			return otherDed;
		}
		public void setOtherDed(String otherDed) {
			this.otherDed = otherDed;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		public String getApprovedBy() {
			return approvedBy;
		}
		public void setApprovedBy(String approvedBy) {
			this.approvedBy = approvedBy;
		}
		public String getDownloadPath() {
			return downloadPath;
		}
		public void setDownloadPath(String downloadPath) {
			this.downloadPath = downloadPath;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getInvFileName() {
			return invFileName;
		}
		public void setInvFileName(String invFileName) {
			this.invFileName = invFileName;
		}
		public String getInvoiceDate() {
			return invoiceDate;
		}
		public void setInvoiceDate(String invoiceDate) {
			this.invoiceDate = invoiceDate;
		}
		public String getNarration() {
			return narration;
		}
		public void setNarration(String narration) {
			this.narration = narration;
		}
		public String getInvoiceType() {
			return invoiceType;
		}
		public void setInvoiceType(String invoiceType) {
			this.invoiceType = invoiceType;
		}
		
}
