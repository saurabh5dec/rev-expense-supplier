package com.fcm.supplier.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fcm.supplier.entity.SupplierForm;
import com.fcm.supplier.repository.SupplierRepository;
import com.fcm.supplier.service.SupplierService;
@Service
public class SupplierServiceImpl implements SupplierService {

	@Autowired
	SupplierRepository supplierRepository;
	
	public void save(SupplierForm supplierForm) {
		// TODO Auto-generated method stub
		supplierRepository.save(supplierForm);
	}

	public List<SupplierForm> findByEmployeeId(String employeeId) {
		// TODO Auto-generated method stub
		return supplierRepository.findByEmployeeId(employeeId);
	}

	@Override
	public SupplierForm findByRefNo(String refNo) {
		// TODO Auto-generated method stub
		return supplierRepository.findByRefNo(refNo);
	}
	
	@Override
	public List<SupplierForm> findBySupplierForm(String fromDate, String toDate, String refNo, String paymentStatus,
			String employeeId) {
		// TODO Auto-generated method stub
		return supplierRepository.findSupplierFormBydueDateAndsubdateAndARefNoAndPaymentStatusAndEmployeeId(fromDate, toDate, refNo, paymentStatus, employeeId);
	}

}
