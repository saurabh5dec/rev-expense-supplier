package com.fcm.supplier.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.fcm.supplier.entity.SupplierForm;
@Repository
public interface SupplierRepository extends JpaRepository<SupplierForm, Long> {

	List<SupplierForm> findByEmployeeId(String employeeId);

	SupplierForm findByRefNo(String refNo);
	 @Query("SELECT p FROM SupplierForm p WHERE (COALESCE(:dueDate) is null or p.dueDate IN (:dueDate)) and (COALESCE(:subDate) is null or p.subDate IN (:subDate)) and (COALESCE(:refNo) is null or p.refNo IN (:refNo)) or (COALESCE(:paymentStatus) is null or p.paymentStatus IN (:paymentStatus)) or (COALESCE(:employeeId) is null or p.employeeId IN (:employeeId))")
	List<SupplierForm> findSupplierFormBydueDateAndsubdateAndARefNoAndPaymentStatusAndEmployeeId(@Param("dueDate") String dueDate,
	                                                                      @Param("subDate") String subDate,
	                                                                      @Param("refNo") String refNo,
	                                                                      @Param("paymentStatus") String paymentStatus,
	                                                                      @Param("employeeId") String employeeId);

}
