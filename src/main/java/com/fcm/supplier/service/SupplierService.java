package com.fcm.supplier.service;

import java.util.List;

import com.fcm.supplier.entity.SupplierForm;

public interface SupplierService {
	public void save(SupplierForm supplierForm);
	public List<SupplierForm> findByEmployeeId(String employeeId);
	public SupplierForm findByRefNo(String refNo);
	 public List<SupplierForm> findBySupplierForm(String fromDate, String toDate, String refNo, String paymentStatus,
				String employeeId);
}
