package com.fcm.supplier.service;

import java.util.Optional;

import com.fcm.supplier.entity.User;



public interface UserService {
	
    Optional<User> findById(Long id);
	public String getRandomString(int n);
	public Optional<User> findByEmployeeId(String employeeId);
	
}
