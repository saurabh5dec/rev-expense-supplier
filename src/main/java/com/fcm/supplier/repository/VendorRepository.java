package com.fcm.supplier.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fcm.supplier.entity.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long> {

	Vendor findByVendorEmail(String vendorEmail);
}