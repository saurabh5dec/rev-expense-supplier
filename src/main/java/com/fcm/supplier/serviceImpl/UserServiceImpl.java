package com.fcm.supplier.serviceImpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fcm.supplier.entity.User;
import com.fcm.supplier.repository.UserRepository;
import com.fcm.supplier.service.UserService;




@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	
	public Optional<User> findById(Long id) {
		// TODO Auto-generated method stub
		System.out.println("value of id"+id);
		return userRepository.findById(id);
	}

	public String getRandomString(int n) {
//		String AlphaNumericString = "ABCDEF#GHIJKL@MNOPQR1233456STUVW$XYZ@" + "0123456789" + "#@$&"
//				+ "abcdefghijk123967lmnop#qrst$uvxyz";
		String AlphaNumericString="0123456789";
		StringBuilder sb = new StringBuilder(n);
		for (int i = 0; i < n; i++) {
			int index = (int) (AlphaNumericString.length() * Math.random());

			sb.append(AlphaNumericString.charAt(index));
		}

		return sb.toString();
	}

	public Optional<User> findByEmployeeId(String employeeId) {
		// TODO Auto-generated method stub
		return userRepository.findByEmployeeId(employeeId);
	}
	
	

}
